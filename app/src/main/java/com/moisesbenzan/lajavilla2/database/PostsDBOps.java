package com.moisesbenzan.lajavilla2.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.moisesbenzan.lajavilla2.models.AuthorModel;
import com.moisesbenzan.lajavilla2.models.PostModel;

import java.util.ArrayList;

/**
 * Created by User on 2/19/2017.
 */

public class PostsDBOps extends SQLiteOpenHelper {
    private static final int DBVersion = 1;
    private static final String DBName = "noticias.db";
    private static final String TAG = "PostsDBOPS";

    //Define the SCHEMA of the DB.
    private static final String TABLENAME = "noticias";
    private static final String COL_POST_ID = "noticias_pID";
    private static final String COL_TITLE = "noticias_pTitle";
    private static final String COL_DATE = "noticias_pDate";
    private static final String COL_EXCERPT = "noticias_pSummary";
    private static final String COL_LINK = "noticias_pLink";
    private static final String COL_CONTENT = "noticias_pContent";
    private static final String COL_AUTHOR_NAME = "noticias_pAutor";
    private static final String COL_AUTHOR_ID = "noticias_pIdAutor";
    private static final String COL_IMAGE_URL = "noticias_pImgUrl";
    private static final String COL_PRIMARY_IMAGE_URL = "noticias_pImgUrl_primary";

    //Predefined SQL commands.
    private static final String SELECT_ALL = "SELECT * FROM " + TABLENAME;
    private static final String SELECT_TITLE = "SELECT " + COL_TITLE + " FROM " + TABLENAME;
    private static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLENAME;

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLENAME
            + "( id INTEGER NOT NULL PRIMARY KEY ASC," +
            COL_POST_ID + " TEXT NOT NULL UNIQUE," +
            COL_TITLE + " TEXT NOT NULL," +
            COL_EXCERPT + " TEXT NOT NULL," +
            COL_LINK + " TEXT NOT NULL," +
            COL_DATE + " TEXT NOT NULL," +
            COL_AUTHOR_NAME + " TEXT NOT NULL," +
            COL_AUTHOR_ID + " TEXT NOT NULL," +
            COL_CONTENT + " TEXT NOT NULL," +
            COL_PRIMARY_IMAGE_URL + " TEXT NOT NULL," +
            COL_IMAGE_URL + " TEXT NOT NULL)";


    public PostsDBOps(Context context) {
        super(context, DBName, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(DELETE_TABLE);
        onCreate(db);
    }


    //CRUD START

    //Insert

    private boolean insertPost(PostModel post) {

        //Runtime variable
        long status = -1;

        //Gets a writeable DB
        SQLiteDatabase database = this.getWritableDatabase();

        //Fills in the data to be inserted
        ContentValues values = new ContentValues();
        values.put(COL_TITLE, post.getTitle());
        values.put(COL_CONTENT, post.getContent());
        values.put(COL_DATE, post.getDate());
        values.put(COL_EXCERPT, post.getExcerpt());
        values.put(COL_LINK, post.getLink());
        values.put(COL_POST_ID, post.getPostId());
        values.put(COL_AUTHOR_ID, post.getAuthor().getId());
        values.put(COL_AUTHOR_NAME, post.getAuthor().getName());
        values.put(COL_IMAGE_URL, post.getEncodedImgURLS());
        values.put(COL_PRIMARY_IMAGE_URL, post.getFirstImgURL());

        //Inserts into db
        try {
            status = database.insertOrThrow(TABLENAME, "-1", values);
        } catch (SQLiteConstraintException e) {
            status = -1;
            Log.d(TAG, "Entry already exists.");
        }

        //Checks and returns
        database.close();
        return status != -1;

    }

    public int insertBatchPosts(ArrayList<PostModel> posts) {
        int count = 0;
        Boolean status = true;
        for (PostModel p : posts) {
            status = insertPost(p);
            if (status) {
                count++;
            }

        }

        return count;
    }
    //Get

    public ArrayList<PostModel> getAllPosts() {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<PostModel> results = new ArrayList<>();

        //Performs the request
        Cursor res = database.rawQuery(SELECT_ALL, null);
        res.moveToFirst();

        // Processes the data
        while (!res.isAfterLast()) {
            String title, link, date, excerpt, content, id, a_id, a_name, img_url, primary_img;

            title = res.getString(res.getColumnIndex(COL_TITLE));
            img_url = res.getString(res.getColumnIndex(COL_IMAGE_URL));
            link = res.getString(res.getColumnIndex(COL_LINK));
            date = res.getString(res.getColumnIndex(COL_DATE));
            excerpt = res.getString(res.getColumnIndex(COL_EXCERPT));
            content = res.getString(res.getColumnIndex(COL_CONTENT));
            id = res.getString(res.getColumnIndex(COL_POST_ID));
            a_name = res.getString(res.getColumnIndex(COL_AUTHOR_NAME));
            a_id = res.getString(res.getColumnIndex(COL_AUTHOR_ID));
            primary_img = res.getString(res.getColumnIndex(COL_PRIMARY_IMAGE_URL));
            AuthorModel author = new AuthorModel(a_id, a_name);

            //Creates object from the data
            PostModel p = new PostModel(id, title, date, link, excerpt, content, author);
            p.setPrimaryImage(primary_img);
            p.addEncodedImageURLS(img_url);

            //Adds the object to the array
            results.add(p);
            res.moveToNext();
        }

        //Returns the ArrayList
        database.close();
        return results;

    }

    public ArrayList<String> getAllPostsTitles() {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<String> results = new ArrayList<>();

        //Performs the request
        Cursor res = database.rawQuery(SELECT_TITLE, null);
        res.moveToFirst();

        //Process the data
        while (!res.isAfterLast()) {
            String title = res.getString(res.getColumnIndex(COL_TITLE));
            results.add(title);
            res.moveToNext();
        }
        database.close();
        return results;
    }

    //Update


    //Delete
}



