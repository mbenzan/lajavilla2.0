package com.moisesbenzan.lajavilla2.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.moisesbenzan.lajavilla2.models.LeagueModel;

import java.util.ArrayList;

/**
 * Created by User on 2/19/2017.
 */

public class LeaguesDBOps extends SQLiteOpenHelper {
    private static final int DBVersion = 1;
    private static final String DBName = "leagues.db";
    private static final String TAG = "LeaguesDBOPS";

    //Define the SCHEMA of the DB.
    private static final String TABLENAME = "torneos";
    private static final String COL_LEAGUE_ID = "torneos_tID";
    private static final String COL_LEAGUE = "torneos_tName";
    private static final String COL_LEAGUE_LINK = "torneos_tLink";

    //Predefined SQL commands.
    private static final String SELECT_ALL = "SELECT * FROM " + TABLENAME;
    private static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLENAME;
    private static final String SELECT_CONDITION = "SELECT * FROM " + TABLENAME + " WHERE " + COL_LEAGUE_ID + "=?";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLENAME
            + "( id INTEGER NOT NULL PRIMARY KEY ASC," +
            COL_LEAGUE_ID + " TEXT NOT NULL UNIQUE," +
            COL_LEAGUE + " TEXT NOT NULL," +
            COL_LEAGUE_LINK + " TEXT NOT NULL"
            + ")";


    public LeaguesDBOps(Context context) {
        super(context, DBName, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(DELETE_TABLE);
        onCreate(db);
    }


    //CRUD START

    //Insert

    private boolean insertLeague(LeagueModel leagueModel) {

        //Runtime variable
        long status = -1;

        //Gets a writeable DB
        SQLiteDatabase database = this.getWritableDatabase();

        //Fills in the data to be inserted
        ContentValues values = new ContentValues();
        values.put(COL_LEAGUE, leagueModel.getName());
        values.put(COL_LEAGUE_ID, leagueModel.getId());
        values.put(COL_LEAGUE_LINK, leagueModel.getUnformattedLink());


        //Inserts into db
        try {
            status = database.insertOrThrow(TABLENAME, null, values);
        } catch (SQLiteConstraintException e) {
            status = -1;
            Log.d(TAG, "Entry already exists.");
            Log.d(TAG, "Error:" + e.getLocalizedMessage());
        }

        //Checks and returns
        database.close();
        return status != -1;

    }

    public boolean insertBatchLeagues(ArrayList<LeagueModel> leagueModels) {
        boolean status = true;
        for (LeagueModel p : leagueModels) {
            status = insertLeague(p);
        }

        return status;
    }
    //Get

    public ArrayList<LeagueModel> getAllLeagues() {
        SQLiteDatabase database = this.getReadableDatabase();
        ArrayList<LeagueModel> results = new ArrayList<>();

        //Performs the request
        Cursor res = database.rawQuery(SELECT_ALL, null);
        res.moveToFirst();

        // Processes the data
        while (!res.isAfterLast()) {
            String id, name, link;

            id = res.getString(res.getColumnIndex(COL_LEAGUE_ID));
            link = res.getString(res.getColumnIndex(COL_LEAGUE_LINK));
            name = res.getString(res.getColumnIndex(COL_LEAGUE));

            //Creates object from the data
            LeagueModel league = new LeagueModel(id, name, link);

            //Adds the object to the array
            results.add(league);
            res.moveToNext();
        }

        //Returns the ArrayList
        database.close();
        return results;

    }

    public LeagueModel getLeagueByID(String id) {
        SQLiteDatabase database = this.getReadableDatabase();
        LeagueModel model = new LeagueModel();

        //Perform the query
        String query = SELECT_CONDITION.replace("?", id);
        Log.d(TAG, "sql query: " + query);

        Cursor res = database.rawQuery(query, null);

        // Processes the data

        while (!res.isAfterLast()) {
            String league_id, name, link;

            league_id = res.getString(res.getColumnIndex(COL_LEAGUE_ID));
            link = res.getString(res.getColumnIndex(COL_LEAGUE_LINK));
            name = res.getString(res.getColumnIndex(COL_LEAGUE));

            //Creates object from the data
            LeagueModel mModel = new LeagueModel(league_id, name, link);
            Log.d(TAG, "Returning model with id: " + mModel.getId());

            //Adds the object to the array
            res.moveToNext();
        }
        res.close();
        database.close();
        return model;
    }

    //Update


    //Delete
}



