package com.moisesbenzan.lajavilla2.models;

import android.text.Html;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 2/19/2017.
 */

public class PostModel {
    private static final String DELIMITER = "-,-";

    private String post_id, author, link, title, content, excerpt, date, author_id, primary_img = "";

    private ArrayList<String> image_url = new ArrayList<>();

    //LOG TAG
    private static final String TAG = "POSTMODEL";

    public PostModel(String id, String title, String pub_date, String link, String excerpt, String content, ArrayList<String> images, String primaryImg, AuthorModel author) {
        this.content = content;
        this.title = title;
        this.link = link;
        this.post_id = id;
        this.excerpt = excerpt;
        this.date = pub_date.replace('T', ' ');
        setImageURLArray(images);
        this.author = author.getName();
        this.author_id = author.getId();
        this.primary_img = primaryImg;

        //Adds the primary img to the array
        this.image_url.add(primaryImg);
    }

    public PostModel(String id, String title, String pub_date, String link, String excerpt, String content) {
        this.content = content;
        this.title = title;
        this.link = link;
        this.post_id = id;
        this.excerpt = excerpt;
        this.date = pub_date.replace('T', ' ');
        this.image_url = getImageFromContent(content);
    }

    public PostModel(String id, String title, String pub_date, String link, String excerpt, String content, String imgUrl, AuthorModel author) {
        this.content = content;
        this.title = title;
        this.link = link;
        this.post_id = id;
        this.excerpt = excerpt;
        this.date = pub_date.replace('T', ' ');
        this.author = author.getName();
        this.author_id = author.getId();
        this.primary_img = getOriginalImageSize(imgUrl);
    }

    public PostModel(String id, String title, String pub_date, String link, String excerpt, String content, AuthorModel author) {
        this.content = content;
        this.title = title;
        this.link = link;
        this.post_id = id;
        this.excerpt = excerpt;
        this.date = pub_date.replace('T', ' ');
        this.author = author.getName();
        this.author_id = author.getId();
        this.image_url = getImageFromContent(content);
    }

    // Getters Methods
    public String getPostId() {
        return this.post_id;
    }

    public String getLink() {
        return this.link;
    }

    public String getTitle() {
        return this.title;
    }

    public String getExcerpt() {
        return this.excerpt;
    }

    public String getFilteredExcerpt() {
        String html = this.excerpt;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }

    public String getContent() {
        return this.content;
    }

    public ArrayList<String> getImageURLArray() {
        ArrayList<String> mUrl = this.image_url;

        //Adds the content images
        for (String s : getImageFromContent(this.content)) {
            mUrl.add(s);
        }
        return mUrl;
    }

    public String getFormattedDate() {
        DateFormat sf = new SimpleDateFormat("MMMM d, y", Locale.US);
        String mDate = this.date;
        try {
            //Parses the Date
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = inputFormat.parse(mDate);

            //Returns the formatted Date
            mDate = sf.format(date);
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }
        return mDate;
    }

    public String getDate() {
        return this.date;
    }

    public String getFirstImgURL() {
        return getOriginalImageSize(this.primary_img);
    }

    public AuthorModel getAuthor() {
        return new AuthorModel(author_id, author);
    }

    //Setters Methods
    public void setPost_id(String pID) {
        this.post_id = pID;
    }

    public void setLink(String nLink) {
        this.link = nLink;
    }

    public void setPrimaryImage(String primImg) {
        this.primary_img = primImg;
    }

    private void setImageURLArray(ArrayList<String> imageUrl) {
        this.primary_img = imageUrl.get(0);
        this.image_url = imageUrl;
    }

    public void setTitle(String nTitle) {
        this.title = nTitle;
    }

    public void setContent(String nContent) {
        this.content = nContent;
    }

    public void addImgURL(String img) {
        this.image_url.add(img);
        if (this.primary_img.isEmpty()) {
            this.primary_img = img;
        }
    }

    public void setExcerpt(String nExcerpt) {
        this.excerpt = nExcerpt;
    }

    public void setAuthor(AuthorModel auth) {
        this.author = auth.getName();
        this.author_id = auth.getId();
    }

    public void setDate(String nDate) {
        nDate.replace('T', ' ');
        this.date = nDate;
    }


    //Public Methods
    public String getFormattedContent() {
        String parsed = this.content.replaceAll("(<(/)img>)|(<img.+?>)", "");
        return Html.fromHtml(parsed).toString();
    }

    public void addEncodedImageURLS(String img) {
        this.image_url = decodeImageURLS(img);
    }

    //Private methods
    private ArrayList<String> getImageFromContent(String c) {
        ArrayList<String> results = new ArrayList<>();
        Pattern pattern = Pattern.compile("src\\s*=\\s*\"([^\"]+)\"");
        Matcher m = pattern.matcher(c);

        while (m.find()) {
            results.add(getOriginalImageSize(m.group(1)));
        }

        if (this.primary_img.isEmpty() && !results.isEmpty()) {
            this.primary_img = results.get(0);
        }

        return results;
    }

    private String getOriginalImageSize(String img) {
        if (img.contains("?")) {
            Pattern p = Pattern.compile("([.]jpg)");
            return p.split(img, 2)[0] + ".jpg";
        } else {
            return img;
        }
    }


    //Encoders
    public String getEncodedImgURLS() {

        String res = this.primary_img + DELIMITER;
        int startingLength = res.length();

        for (String s : this.image_url) {
            res = res.concat(s + DELIMITER);
        }

        if (res.contains(DELIMITER) && res.length() > startingLength) {
            res = res.substring(0, res.length() - 1);
        } else {
            res = res.substring(0, startingLength - DELIMITER.length());
        }
        return res;
    }

    //Decoders
    private ArrayList<String> decodeImageURLS(String img) {

        ArrayList<String> res = new ArrayList<>();

        String[] img_parts = img.split(DELIMITER);

        Collections.addAll(res, img_parts);

        return res;
    }
}
