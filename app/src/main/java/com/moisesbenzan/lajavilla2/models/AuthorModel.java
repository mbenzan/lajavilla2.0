package com.moisesbenzan.lajavilla2.models;

/**
 * Created by User on 2/22/2017.
 */

public class AuthorModel {
    private String name = "";
    private String id = "";

    public AuthorModel(String id, String name) {
        this.name = name;
        this.id = id;
    }

    public AuthorModel() {
    }

    //Getters

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

    //Setters

    public void setName(String nName) {
        this.name = nName;
    }

    public void setId(String nId) {
        this.id = nId;
    }
}
