package com.moisesbenzan.lajavilla2.models;

/**
 * Created by User on 2/22/2017.
 */

public class LeagueModel {
    private String id = "";
    private String name = "";
    private String link = "";
    private String description = "";
    private String slug = "";

    public LeagueModel(String id, String name, String link) {
        this.id = id;
        this.name = name;
        this.link = link;
    }

    public LeagueModel() {

    }

    //Setters
    public void setDescription(String desc) {
        this.description = desc;
    }

    public void setSlug(String s) {
        this.slug = s;
    }

    //Getters

    public String getId() {
        return this.id;
    }

    public String getSlug() {
        return this.slug;
    }

    public String getDescription() {
        return this.description;
    }

    public String getName() {
        return this.name;
    }

    public String getLink() {
        //this.link.replace("/league","")
        return this.link.replace("/league", "/tag");
    }

    public String getUnformattedLink() {
        return this.link;
    }

}
