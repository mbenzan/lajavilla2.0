package com.moisesbenzan.lajavilla2.models;

import android.util.Log;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created by User on 4/4/2017.
 */

public class PlayerModel implements Serializable {
    private final static String DELIMITER = ", ";
    private final static String TAG = "PlayerModel";
    private final static String DEFAULT_IMG = "https://placeimg.com/640/480/people/grayscale";
    private final static String EMPTY_MSG = "N/A";


    private String featured_media = "";
    private String name = "";
    private String link = "";
    private String seasons = "";
    private String current_team = "";
    private String id = "";
    private String number = "";
    private String positions = "";
    private String teams = "";
    private String leagues = "";
    private String nationality = "";
    private String past_teams = "";
    private String fav_player = "";
    private String category = "";
    private String birthDay = "";
    private String BT = "";
    private String birth_place = "";

    public PlayerModel() {

    }

    public PlayerModel(String name) {
        this.name = name;
    }

    public PlayerModel(String name, String link, String seasons, String current_team, String id) {
        this.name = name;
        this.link = link;
        this.seasons = seasons;
        this.current_team = current_team;
        this.id = id;
    }


    //Setters
    public void setFav_player(String fav) {
        this.fav_player = fav;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay.split("T")[0];
    }

    public void setCategory(String cat) {
        this.category = cat;
    }

    public void setBT(String bats_throws) {
        this.BT = bats_throws;
    }

    public void setBirth_place(String birth_place) {
        this.birth_place = birth_place;
    }

    public void setFeatured_media(String f) {
        this.featured_media = f;
    }

    public void addLeague(String league) {
        if (this.leagues == null) {
            this.leagues = league + DELIMITER;
        } else {
            this.leagues += league + DELIMITER;
        }
    }

    public void addCurrent_team(String current_team) {
        if (this.current_team == null) {
            this.current_team = current_team + DELIMITER;
        } else {
            this.current_team += current_team + DELIMITER;
        }
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStats(String stats) {
        String stats1 = stats;
    }

    public void addTeam(String team) {
        if (this.teams == null) {
            this.teams = team + DELIMITER;
        } else {
            this.teams += team + DELIMITER;
        }
    }

    public void addSeason(String season) {
        if (this.seasons == null) {
            this.seasons = season + DELIMITER;
        } else {
            this.seasons += season + DELIMITER;
        }
    }

    public void addNationality(String nationality) {
        if (this.nationality == null) {
            this.nationality = nationality + DELIMITER;
        } else {
            this.nationality += nationality + DELIMITER;
        }
    }

    public void addPosition(String position) {
        if (this.positions == null) {
            this.positions = position + DELIMITER;
        } else {
            this.positions += position + DELIMITER;
        }
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setPast_teams(String past_teams) {
        this.past_teams = past_teams;
    }


    private String getBirthDay() {
        return this.birthDay;
    }

    public String getCurrent_team() {
        if (this.current_team.isEmpty()) {
            return EMPTY_MSG;
        } else {
            return this.current_team.substring(0, this.current_team.length() - DELIMITER.length());
        }
    }

    public String getFeatured_media() {
        if (this.featured_media.isEmpty()) {
            return DEFAULT_IMG;
        }
        return featured_media;
    }

    public String getId() {
        return id;
    }

    public String getLeagues() {
        if (this.leagues.isEmpty()) {
            return EMPTY_MSG;
        } else {
            return this.leagues.substring(0, this.leagues.length() - DELIMITER.length());
        }
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getNationality() {
        if (this.nationality.isEmpty()) {
            return EMPTY_MSG;
        } else {
            return this.nationality.substring(0, this.nationality.length() - DELIMITER.length());
        }
    }

    public String getNumber() {
        return number;
    }

    public String getPast_teams() {
        if (this.past_teams.isEmpty()) {
            return EMPTY_MSG;
        } else {
            return this.past_teams.substring(0, this.past_teams.length() - DELIMITER.length());
        }
    }

    public String getPositions() {
        if (this.positions.isEmpty()) {
            return EMPTY_MSG;
        } else {
            return this.positions.substring(0, this.positions.length() - DELIMITER.length());
        }
    }

    private String getSeasons() {
        if (this.seasons.isEmpty()) {
            return EMPTY_MSG;
        } else {
            return this.seasons.substring(0, this.seasons.length() - DELIMITER.length());
        }
    }

    public String getTeams() {
        if (this.teams.isEmpty()) {
            return EMPTY_MSG;
        } else {
            return this.teams.substring(0, this.teams.length() - DELIMITER.length());
        }
    }

    public String getBirth_place() {
        if (this.birthDay.isEmpty()) {
            return EMPTY_MSG;
        } else {
            return this.birth_place;
        }
    }

    public String getBT() {
        if(this.BT.isEmpty()){
            return EMPTY_MSG;
        } else {
            return this.BT;
        }
    }

    public String getCategory() {
        if (this.category.isEmpty()){
            return EMPTY_MSG;
        } else {
            return this.category;
        }
    }

    public String getFav_player() {
        if (this.fav_player.isEmpty()){
            return EMPTY_MSG;
        } else {
            return this.fav_player;
        }
    }

    public String getFormattedBirthday() {
        if (this.birthDay.isEmpty()) {
            return EMPTY_MSG;
        }
        DateFormat sf = new SimpleDateFormat("MMMM d, y", Locale.US);
        String mDate = this.birthDay;
        try {
            //Parses the Date
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date = inputFormat.parse(mDate);

            //Returns the formatted Date
            mDate = sf.format(date);
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }
        return mDate;
    }

    public String getAge() {

        if (this.birthDay.isEmpty()) {
            return "0";
        }

        String[] dob = this.getBirthDay().split("-");

        Calendar cal = Calendar.getInstance();

        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);

        cal.set(Integer.valueOf(dob[0]), Integer.valueOf(dob[1]) - 1, Integer.valueOf(dob[2]));

        a = y - cal.get(Calendar.YEAR);

        if ((m < cal.get(Calendar.MONTH)) || ((m == cal.get(Calendar.MONTH)) && (d < cal.get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if (a < 0)
            throw new IllegalArgumentException("Age < 0");

        Integer age = a;
        return age.toString();
    }

    public ArrayList<String> getPositionsList() {
        ArrayList<String> out = new ArrayList<>();

        String[] positions = getPositions().split(DELIMITER);

        Collections.addAll(out, positions);

        return out;
    }

    public ArrayList<String> getTournamentsList() {
        ArrayList<String> out = new ArrayList<>();

        String[] tournaments = getSeasons().split(DELIMITER);

        Collections.addAll(out, tournaments);

        return out;
    }

}
