package com.moisesbenzan.lajavilla2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.Tab;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.moisesbenzan.lajavilla2.adapters.MainViewPagerAdapter;
import com.moisesbenzan.lajavilla2.adapters.MarqueeScrollingAdapter;
import com.moisesbenzan.lajavilla2.interfaces.APIManager;
import com.moisesbenzan.lajavilla2.services.AlarmReceiver;
import com.moisesbenzan.lajavilla2.services.SearchService;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MAINACTIVITY";
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    ArrayList<String> mDataset;
    private TabLayout mTabs;
    private ViewPager mViewPager;
    private AppBarLayout mAppBarLayout;
    private EditText searchBox;
    private RecyclerView mMarqueeView;
    private final Runnable SCROLLING_RUNNABLE = new Runnable() {

        @Override
        public void run() {
            final int duration = 10;
            final int pixelsToMove = 10;
            mMarqueeView.smoothScrollBy(pixelsToMove, 0);
            mHandler.postDelayed(this, duration);
        }
    };
    //Marquee view settings
    //private boolean loading = true;
    private boolean foundTotalPixel = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int totalMovedPixel;
    private int totalPixel;
    private APIManager apiManager;
    private Boolean mFirstRun = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ActionBar mActionBarHolder;
        apiManager = new APIManager(this);

        // Checks if its a first time run
        mFirstRun = firstRunCheck();
        if (mFirstRun) {
            apiManager.updateAllDB();
            Log.d(TAG, "First run detected or all data deleted. Triggering all db updates.");
        }

        //Gets a handle of the appbar layout
        mAppBarLayout = (AppBarLayout) findViewById(R.id.main_activity_appbar_layout);

        //Sets up some variables and finds the toolbar
        CharSequence mDrawerTitle;
        CharSequence mTitle = mDrawerTitle = getTitle();
        Toolbar mToolBar = (Toolbar) findViewById(R.id.main_activity_primary_toolbar);

        //Gets a handle of the searchbox
        searchBox = (EditText) findViewById(R.id.main_activity_primary_toolbar_edittext);

        //Sets the listener
        searchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionID, KeyEvent keyEvent) {
                boolean handled = false;

                if (actionID == EditorInfo.IME_ACTION_SEARCH) {
                    //search for player and change activity
                    searchForPlayerAction(textView.getText().toString().trim());

                    //Returns handled
                    handled = true;
                }

                return handled;
            }
        });


        //
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.main_activity_collapsing_toolbar);
        collapsingToolbarLayout.setTitle("");
        collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(android.R.color.transparent));
        //collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        //we set it
        setSupportActionBar(mToolBar);
        mActionBarHolder = getSupportActionBar();
        mActionBarHolder.setTitle("");
        mActionBarHolder.setLogo(R.mipmap.ic_launcher);

        //Get a handle for the ViewPager
        mViewPager = (ViewPager) findViewById(R.id.main_activity_viewpager);

        //Creates the ViewPager Adapter
        MainViewPagerAdapter viewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(viewPagerAdapter);

        //Get a handle of the tab layout and fill tabs from viewpager
        mTabs = (TabLayout) findViewById(R.id.main_activity_tabs);
        mTabs.setupWithViewPager(mViewPager);

        //Set swipe listener
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int pos) {
                mTabs.getTabAt(pos).select(); //Selects the tab at positions
            }
        });

        //Sets the tab listener
        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition(), true);

                //Collapses the toolbar on the About page
                if (tab.getPosition() == 2) {
                    mAppBarLayout.setExpanded(false, true);
                } else {
                    mAppBarLayout.setExpanded(true, true);
                }
            }

            @Override
            public void onTabUnselected(Tab tab) {

            }

            @Override
            public void onTabReselected(Tab tab) {

            }
        });

        // Marquee (Scrolling) view header.
        mMarqueeView = (RecyclerView) findViewById(R.id.main_activity_marquee_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mMarqueeView.setLayoutManager(layoutManager);
        mDataset = apiManager.getPostsTitleArray();

        mMarqueeView.setAdapter(new MarqueeScrollingAdapter(mDataset));


        //Scroll listener

        mMarqueeView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalMovedPixel = totalMovedPixel + dx;
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                if (foundTotalPixel) {
                    if (totalItemCount > 2) {
                        View headerView = layoutManager.getChildAt(0);
                        View itemView = layoutManager.getChildAt(1);

                        if (itemView != null && headerView != null) {
                        /*total visible scrolling part is total pixel's of total item's count and header view*/
                            totalPixel = /*-c.getTop() +*/ ((totalItemCount - 2) * itemView.getWidth()) + (1 * headerView.getWidth());
                            Log.v("...", "Total pixel x!" + totalPixel);
                            foundTotalPixel = false;
                        }
                    }
                }

                //if (loading) {
                //if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                if (!foundTotalPixel && totalMovedPixel >= totalPixel) {
                    // use this to turn auto-scrolling off:
                    //mHandler.removeCallbacks(SCROLLING_RUNNABLE);
                    mDataset = apiManager.getPostsTitleArray();
                    mMarqueeView.setAdapter(new MarqueeScrollingAdapter(mDataset));
                    pastVisiblesItems = visibleItemCount = totalItemCount = 0;
                    totalMovedPixel = 0;

                }
            }
            // }
        });

        // Starts the scrolling behavior
        mHandler.post(SCROLLING_RUNNABLE);


        //Broadcast listener...refreshes the viewPage
        final Context c = this;
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(c, R.string.local_broadcast_update_received, Toast.LENGTH_SHORT).show();
                refreshViewPager();
            }
        };
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(receiver, new IntentFilter(getString(R.string.ALLUPDATEBROADCAST)));


        //Sets up the recurrent alarm if it is not already set
        //Alarm will run twice a day
        Intent alarm = new Intent(this, AlarmReceiver.class);
        alarm.setAction("UPDATEALL");
        boolean alarmRunning = (PendingIntent.getBroadcast(c, 0, alarm, PendingIntent.FLAG_NO_CREATE) != null);
        if (!alarmRunning) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, 0, alarm, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_HALF_DAY, AlarmManager.INTERVAL_HALF_DAY, pendingIntent);
            Log.d(TAG, "Registered recurring alarm.");
        }

    }

    private void searchForPlayerAction(String playerName) {
        Log.d(TAG, "Searching for: " + playerName);

        //Closes the keyboard
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(searchBox.getWindowToken(), 0);

        //Hides the searchbox
        searchBox.clearFocus();
        searchBox.setText("");
        searchBox.setVisibility(View.GONE);

        //Checks if input is not empty.
        if (!playerName.isEmpty()) {
            //searches for the player (Repeated twice for more durability)
            String msg = getString(R.string.searching_for_message).replace("{}", playerName);
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            SearchService.startSearchForPlayer(this, playerName);

            //Restores the marquee view
            searchBox.clearFocus();
            searchBox.setVisibility(View.GONE);
            searchBox.setElevation(0);
            mMarqueeView.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(this, R.string.search_empty_box, Toast.LENGTH_SHORT).show();
            searchBox.clearFocus();
            searchBox.setVisibility(View.GONE);
            searchBox.setElevation(0);

            //Restores the marquee view
            mMarqueeView.setVisibility(View.VISIBLE);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_primary_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_search) {
            if (!searchBox.isFocused()) {
                //Hides the marquee view
                mMarqueeView.setVisibility(View.GONE);

                //Shows the search box
                searchBox.setElevation(5);
                searchBox.setVisibility(View.VISIBLE);
                searchBox.requestFocusFromTouch();

            } else {
                searchForPlayerAction(searchBox.getText().toString());
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshViewPager() {
        //Re-sets the view pager adapter to cause a refresh
        MainViewPagerAdapter viewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(viewPagerAdapter);
    }


    private Boolean firstRunCheck() {
        SharedPreferences sharedPreferences = getSharedPreferences("LaJavilla", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        boolean firstTime = sharedPreferences.getBoolean("first", true);
        if (firstTime) {
            editor.putBoolean("first", false).apply();
            return true;
        } else {
            return false;
        }
    }
}
