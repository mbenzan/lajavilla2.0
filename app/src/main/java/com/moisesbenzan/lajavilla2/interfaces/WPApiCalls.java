package com.moisesbenzan.lajavilla2.interfaces;

import android.util.JsonReader;
import android.util.Log;

import com.moisesbenzan.lajavilla2.models.AuthorModel;
import com.moisesbenzan.lajavilla2.models.LeagueModel;
import com.moisesbenzan.lajavilla2.models.PlayerModel;
import com.moisesbenzan.lajavilla2.models.PostModel;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 2/19/2017.
 */

public class WPApiCalls {
    private static final String TAG = "WPApiCall";
    private static final String posts_endpoint = "/wp/v2/posts";
    private static final String authorInfo_endpoint = "/wp/v2/users/?";
    private static final String leagues_endpoint = "/leagues/?";
    private static final String all_leagues_endpoint = "/leagues/&per_page=100";
    private static final String players_endpoint = "/players&per_page={}";
    private static final String media_endpoint = "/wp/v2/media/?";
    private static final String positions_endpoint = "/positions/?";
    private static final String seasons_endpoint = "/seasons/?";
    private static String base_url = "http://academialajavilla.com/?rest_route=";
    private static final String sportsPress_base_url = base_url + "/sportspress/v2";

    public WPApiCalls(String baseUrl) {
        base_url = baseUrl;
    }

    public WPApiCalls() {
    }

    private String getSeasonNameFromID(String id) {
        LeagueModel model = getSeasonFromID(id);
        if (model != null) {
            //Prefer description over slug over name
            if (!model.getSlug().isEmpty()) {
                return "Temporada " + model.getSlug();
            } else if (!model.getDescription().isEmpty()) {
                return model.getDescription();
            } else {
                return model.getName();
            }

        } else {
            return "";
        }
    }

    public String getLeagueNameFromID(String id) {
        LeagueModel model = getLeagueFromID(id);
        if (model != null) {
            //Prefer description over name
            if (!model.getDescription().isEmpty()) {
                return model.getDescription();
            } else {
                return model.getName();
            }

        } else {
            return "";
        }
    }


    public ArrayList<PlayerModel> getPlayerInfo(String player_name) {
        ArrayList<PlayerModel> processed = new ArrayList<>();
        String requestUrl;
        String num_results = "20"; //Number of results returned by the api

        try {
            requestUrl = sportsPress_base_url + players_endpoint.replace("{}", num_results) + "&search=" + URLEncoder.encode(player_name, "utf-8");
        } catch (Exception e) {
            Log.d(TAG, "Error urlEncoding: " + e.getMessage());
            return processed;
        }

        try {
            URL postEndpoint = new URL(requestUrl);

            HttpURLConnection connection = (HttpURLConnection) postEndpoint.openConnection();
            connection.setRequestProperty("User-Agent", "LaJavilla-Mobile-App");

            //Checks  if request successful
            if (connection.getResponseCode() == 200) {
                InputStream res = connection.getInputStream();
                InputStreamReader resReader = new InputStreamReader(res, "UTF-8");

                //Little hack because of server errors

                JsonReader reader = new JsonReader(resReader);

                processed = parsePlayerFromReader(reader);

            } else {
                Log.e(TAG, "Error retrieving response. No internet maybe?");
            }

        } catch (Exception e) {
            Log.e(TAG, "Got error: " + e);
            e.printStackTrace();
        }

        return processed;

    }

    private String getPositionNameFromID(String pos_id) {
        String processed = "";
        String requestUrl;

        try {
            requestUrl = sportsPress_base_url + positions_endpoint.replace("?", URLEncoder.encode(pos_id, "utf-8"));
        } catch (Exception e) {
            Log.d(TAG, "Error urlEncoding: " + e.getMessage());
            return processed;
        }

        try {
            URL postEndpoint = new URL(requestUrl);

            HttpURLConnection connection = (HttpURLConnection) postEndpoint.openConnection();
            connection.setRequestProperty("User-Agent", "LaJavilla-Mobile-App");

            //Checks  if request successful
            if (connection.getResponseCode() == 200) {
                InputStream res = connection.getInputStream();
                InputStreamReader resReader = new InputStreamReader(res, "UTF-8");

                JsonReader reader = new JsonReader(resReader);

                processed = parsePositionNameFromReader(reader);

            } else {
                Log.e(TAG, "Error retrieving response. No internet maybe?");
            }

        } catch (Exception e) {
            Log.e(TAG, "Got error: " + e);
            e.printStackTrace();
        }

        return processed;
    }

    public LeagueModel getLeagueFromID(String id) {
        String requestUrl = sportsPress_base_url + leagues_endpoint.replace("?", id);
        LeagueModel model = null;
        try {
            URL postEndpoint = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) postEndpoint.openConnection();
            connection.setRequestProperty("User-Agent", "LaJavilla-Mobile-App");

            //Checks  if request successful
            if (connection.getResponseCode() == 200) {
                InputStream res = connection.getInputStream();
                InputStreamReader resReader = new InputStreamReader(res, "UTF-8");

                JsonReader reader = new JsonReader(resReader);

                model = parseLeagueFromReader(reader).get(0);

            } else {
                Log.e(TAG, "Error retrieving response. No internet maybe?");
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Malformed URL. Cannot Process");
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Error connecting. No internet maybe?");
            Log.e(TAG, e.getMessage());
        }

        return model;

    }

    private LeagueModel getSeasonFromID(String id) {
        String requestUrl = sportsPress_base_url + seasons_endpoint.replace("?", id);
        LeagueModel model = null;
        try {
            URL postEndpoint = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) postEndpoint.openConnection();
            connection.setRequestProperty("User-Agent", "LaJavilla-Mobile-App");

            //Checks  if request successful
            if (connection.getResponseCode() == 200) {
                InputStream res = connection.getInputStream();
                InputStreamReader resReader = new InputStreamReader(res, "UTF-8");

                JsonReader reader = new JsonReader(resReader);

                model = parseLeagueFromReader(reader).get(0);

            } else {
                Log.e(TAG, "Error retrieving response. No internet maybe?");
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Malformed URL. Cannot Process");
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Error connecting. No internet maybe?");
            Log.e(TAG, e.getMessage());
        }

        return model;

    }

    public ArrayList<LeagueModel> getLeagues() {
        String requestUrl = sportsPress_base_url + all_leagues_endpoint;
        ArrayList<LeagueModel> processed = new ArrayList<>();
        try {
            URL postEndpoint = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) postEndpoint.openConnection();
            connection.setRequestProperty("User-Agent", "LaJavilla-Mobile-App");

            //Checks  if request successful
            if (connection.getResponseCode() == 200) {
                InputStream res = connection.getInputStream();
                InputStreamReader resReader = new InputStreamReader(res, "UTF-8");

                JsonReader reader = new JsonReader(resReader);


                processed = parseLeaguesFromReader(reader, false);

            } else {
                Log.e(TAG, "Error retrieving response. No internet maybe?");
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Malformed URL. Cannot Process");
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Error connecting. No internet maybe?");
            Log.e(TAG, e.getMessage());
        }

        return processed;

    }

    private AuthorModel getAuthorModelFromID(String id) {
        String requestUrl = base_url + authorInfo_endpoint.replace("?", id);
        AuthorModel processed = new AuthorModel();

        try {
            URL postEndpoint = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) postEndpoint.openConnection();
            connection.setRequestProperty("User-Agent", "LaJavilla-Mobile-App");

            //Checks  if request successful
            if (connection.getResponseCode() == 200) {
                InputStream res = connection.getInputStream();
                InputStreamReader resReader = new InputStreamReader(res, "UTF-8");

                JsonReader reader = new JsonReader(resReader);


                processed = parseAuthorFromReader(reader);

            } else {
                Log.e(TAG, "Error retrieving response. No internet maybe?");
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Malformed URL. Cannot Process");
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Error connecting. No internet maybe?");
            Log.e(TAG, e.getMessage());
        }

        return processed;
    }


    public ArrayList<PostModel> getRecentPosts() {
        String requestUrl = base_url + posts_endpoint;
        ArrayList<PostModel> processed = new ArrayList<>();
        try {
            URL postEndpoint = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) postEndpoint.openConnection();
            connection.setRequestProperty("User-Agent", "LaJavilla-Mobile-App");

            //Checks  if request successful
            if (connection.getResponseCode() == 200) {
                InputStream res = connection.getInputStream();
                InputStreamReader resReader = new InputStreamReader(res, "UTF-8");

                JsonReader reader = new JsonReader(resReader);


                processed = parsePostModelFromReader(reader);

            } else {
                Log.e(TAG, "Error retrieving response. No internet maybe?");
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Malformed URL. Cannot Process");
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Error connecting. No internet maybe?");
            Log.e(TAG, e.getMessage());
        }

        return processed;

    }

    private String getImageFromFeatured(String mID) {
        String processed = "";
        String requestUrl;

        try {
            requestUrl = base_url + media_endpoint.replace("?", URLEncoder.encode(mID, "utf-8"));
        } catch (Exception e) {
            Log.d(TAG, "Error urlEncoding: " + e.getMessage());
            return processed;
        }

        try {
            URL postEndpoint = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) postEndpoint.openConnection();
            connection.setRequestProperty("User-Agent", "LaJavilla-Mobile-App");

            //Checks  if request successful
            if (connection.getResponseCode() == 200) {
                InputStream res = connection.getInputStream();
                InputStreamReader resReader = new InputStreamReader(res, "UTF-8");

                JsonReader reader = new JsonReader(resReader);


                processed = parseImageFromReader(reader);

            } else {
                Log.e(TAG, "Error retrieving response. No internet maybe?");
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Malformed URL. Cannot Process");
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Error connecting. No internet maybe?");
            Log.e(TAG, e.getMessage());
        }

        return processed;
    }


    //Parsers

    private String parseImageFromReader(JsonReader reader) throws IOException {
        String link = "";

        reader.beginObject();

        while (reader.hasNext() && reader.peek() != null) {
            String curr_key = reader.nextName();
            switch (curr_key) {
                case "source_url":
                    link = reader.nextString();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }

        reader.endObject();

        return link;
    }


    private ArrayList<PostModel> parsePostModelFromReader(JsonReader reader) throws IOException {
        String title = "";
        String id = "";
        String link = "";
        String content = "";
        String excerpt = "";
        String date = "";
        String author = "";
        String current_img = "";
        String primaryImg = "";
        ArrayList<PostModel> results = new ArrayList<>();

        //At Main Array level
        reader.beginArray();

        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                //Descended to second objects level

                final String name = reader.nextName();
                final boolean isNull = reader.peek() == null;

                // Process third level data
                if (!isNull) {
                    switch (name) {
                        case "id":
                            id = reader.nextString();
                            break;
                        case "author":
                            author = reader.nextString();
                            break;
                        case "link":
                            link = reader.nextString();
                            break;
                        case "date":
                            date = reader.nextString();
                            break;
                        case "content":
                            //Enters fourth level data
                            reader.beginObject();
                            while (reader.hasNext()) {
                                final String fourthLevel = reader.nextName();
                                final boolean fourthLevelIsNull = reader.peek() == null;

                                if (!fourthLevelIsNull && fourthLevel.equals("rendered")) {
                                    //Process data
                                    content = reader.nextString();

                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                            //Exits fourth level
                            break;
                        case "excerpt":
                            //Enters fourth level data
                            reader.beginObject();
                            while (reader.hasNext()) {
                                final String fourthLevel = reader.nextName();
                                final boolean fourthLevelIsNull = reader.peek() == null;

                                if (!fourthLevelIsNull && fourthLevel.equals("rendered")) {
                                    //Process data
                                    excerpt = reader.nextString();

                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                            //Exits fourth level
                            break;
                        case "title":
                            //Enters fourth level data
                            reader.beginObject();
                            while (reader.hasNext()) {
                                final String fourthLevel = reader.nextName();
                                final boolean fourthLevelIsNull = reader.peek() == null;

                                if (!fourthLevelIsNull && fourthLevel.equals("rendered")) {
                                    //Process data
                                    title = reader.nextString();

                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                            //Exits fourth level
                            break;
                        case "jetpack-related-posts":
                            Boolean flag = true;
                            //Enters fourth level
                            reader.beginArray();
                            //Only interested in the first array object, thus a while loop would
                            //Decrease performance.
                            while (reader.hasNext()) {
                                final boolean fourthLevelIsNull = reader.peek() == null;

                                if (!fourthLevelIsNull) {
                                    //Enters fifth level data
                                    reader.beginObject();

                                    while (reader.hasNext()) {
                                        final String fifthLevel = reader.nextName();
                                        final boolean fifthLevelIsNull = reader.peek() == null;


                                        if (!fifthLevelIsNull && fifthLevel.equals("img")) {
                                            //Enters sixth level data
                                            reader.beginObject();
                                            while (reader.hasNext()) {
                                                final String sixthLevel = reader.nextName();
                                                final boolean sixthLevelIsNull = reader.peek() == null;

                                                if (!sixthLevelIsNull && sixthLevel.equals("src")) {
                                                    current_img = reader.nextString();
                                                    if (current_img.contains(".jpg") && !current_img.contains("mp4")) {
                                                        if (flag) {
                                                            primaryImg = current_img;
                                                            flag = false;
                                                        }
                                                    }
                                                } else {
                                                    reader.skipValue();
                                                }
                                            }
                                            //Exists sixth level data
                                            reader.endObject();
                                        } else {
                                            reader.skipValue();
                                        }
                                    }
                                    //Exists fifth level data
                                    reader.endObject();
                                } else {
                                    reader.skipValue();
                                }
                            }
                            //Exists fourth level
                            reader.endArray();
                            reader.nextName(); //For some reason app crashes without this line
                        default:
                            reader.skipValue();
                            break;
                    }

                } else {
                    reader.skipValue();
                }

            }
            //Exits second level
            reader.endObject();

            //Adds the result to the array
            Log.d(TAG, "Adding one Post to array.");
            AuthorModel authorModel = getAuthorModelFromID(author);
            PostModel p = new PostModel(id, title, date, link, excerpt, content, primaryImg, authorModel);
            results.add(p);
        }
        //Exits first level and closes all
        reader.endArray();

        return results;
    }

    private AuthorModel parseAuthorFromReader(JsonReader reader) throws IOException {
        String id = "";
        String name = "";

        //At Main level
        reader.beginObject();
        while (reader.hasNext()) {
            String first_key = reader.nextName();
            final boolean isNull = reader.peek() == null;
            if (!isNull) {
                switch (first_key) {
                    case "id":
                        id = reader.nextString();
                        break;
                    case "name":
                        name = reader.nextString();
                        break;
                    default:
                        reader.skipValue();
                }
            }
        }
        //Exits main level
        reader.endObject();

        //Creates and returns model
        AuthorModel author = new AuthorModel(id, name);
        Log.d(TAG, "Adding author " + name + " with id: " + id);
        return author;
    }

    private ArrayList<LeagueModel> parseLeaguesFromReader(JsonReader reader, Boolean single_result) throws IOException {
        String id = "";
        String name = "";
        String link = "";
        Boolean flag = true;
        ArrayList<LeagueModel> results = new ArrayList<>();

        //Enter parsing Loop

        //Enters first level
        reader.beginArray();
        while (reader.hasNext()) {
            if (flag) {
                //Enters second level array
                reader.beginObject();

                while (reader.hasNext()) {
                    final String first_key_name = reader.nextName();
                    final boolean isNull = reader.peek() == null;

                    if (!isNull) {
                        switch (first_key_name) {
                            case "name":
                                name = reader.nextString();
                                break;
                            case "id":
                                id = reader.nextString();
                                break;
                            case "link":
                                link = reader.nextString();
                                break;
                            default:
                                reader.skipValue();
                                break;
                        }
                    }

                }

                //Adds object to results
                LeagueModel leagueModel = new LeagueModel(id, name, link);
                Log.d(TAG, "Adding one League to Array");
                results.add(leagueModel);
                if (single_result) {
                    flag = false;
                    Log.d(TAG, "Single result flag reached.");
                }

                //Exists first level
                reader.endObject();
            } else {
                reader.skipValue();
            }
        }
        reader.endArray();

        //Exists parsing loop and returns
        return results;

    }

    private ArrayList<LeagueModel> parseLeagueFromReader(JsonReader reader) throws IOException {
        String id = "";
        String name = "";
        String link = "";
        String description = "";
        String slug = "";
        ArrayList<LeagueModel> results = new ArrayList<>();

        //Enter parsing Loop

        //Enters second level array
        reader.beginObject();

        while (reader.hasNext()) {
            final String first_key_name = reader.nextName();

            switch (first_key_name) {
                case "name":
                    name = reader.nextString();
                    break;
                case "id":
                    id = reader.nextString();
                    break;
                case "link":
                    link = reader.nextString();
                    break;
                case "description":
                    description = reader.nextString();
                    break;
                case "slug":
                    slug = reader.nextString();
                    break;

                default:
                    reader.skipValue();
                    break;
            }
        }

        //Exists first level
        reader.endObject();

        //Adds object to results
        LeagueModel leagueModel = new LeagueModel(id, name, link);
        leagueModel.setDescription(description);
        leagueModel.setSlug(slug);
        Log.d(TAG, "Adding season/league with id: " + leagueModel.getId() + " name: " + leagueModel.getName());
        results.add(leagueModel);

        //Exists parsing loop and returns
        return results;

    }

    private ArrayList<PlayerModel> parsePlayerFromReader(JsonReader reader) throws IOException {
        ArrayList<PlayerModel> results = new ArrayList<>();

        //At main level start
        reader.beginArray();

        while (reader.hasNext() && reader.peek() != null) {
            //Second level start
            reader.beginObject();

            //Creates the current player model
            PlayerModel model = new PlayerModel();

            while (reader.hasNext()) {
                final String first_key_name = reader.nextName();
                final boolean isNull = reader.peek() == null;

                if (!isNull) {
                    switch (first_key_name) {
                        case "id":
                            model.setId(reader.nextString());
                            break;
                        case "link":
                            model.setLink(reader.nextString());
                            break;
                        case "title":
                            reader.beginObject();
                            while (reader.hasNext()) {
                                if (reader.peek() != null) {
                                    final String current_key = reader.nextName();
                                    switch (current_key) {
                                        case "rendered":
                                            model.setName(reader.nextString());
                                            break;
                                        default:
                                            reader.skipValue();
                                    }
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                            break;

                        case "metrics":
                            try {
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    if (reader.peek() != null) {
                                        String key = reader.nextName();
                                        switch (key) {
                                            case "Favorite Player":
                                                model.setFav_player(reader.nextString());
                                                break;
                                            case "Category":
                                                model.setCategory(reader.nextString());
                                                break;
                                            case "Bats / Throws":
                                                model.setBT(reader.nextString());
                                                break;
                                            case "Birthplace":
                                                model.setBirth_place(reader.nextString());
                                                break;
                                            default:
                                                reader.skipValue();
                                        }
                                    } else {
                                        reader.skipValue();
                                    }
                                }
                                reader.endObject();
                            } catch (IllegalStateException ignored){
                                Log.d(TAG, "Player missing info.");
                                reader.beginArray();
                                reader.endArray();
                            }
                            break;

                        case "nationalities":
                            reader.beginArray();
                            while (reader.hasNext()) {
                                if (reader.peek() != null) {
                                    String nationality = reader.nextString();
                                    model.addNationality(nationality);
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endArray();
                            break;

                        case "leagues":
                            reader.beginArray();
                            while (reader.hasNext() && reader.peek() != null) {
                                model.addLeague(reader.nextString());
                            }
                            reader.endArray();
                            break;

                        case "seasons":
                            reader.beginArray();
                            while (reader.hasNext()) {
                                if (reader.peek() != null) {
                                    String curr_id = reader.nextString();
                                    model.addSeason(getSeasonNameFromID(curr_id));
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endArray();
                            break;

                        case "teams":
                            reader.beginArray();
                            while (reader.hasNext()) {
                                if (reader.peek() != null) {
                                    model.addTeam(reader.nextString());
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endArray();
                            break;
                        case "current_teams":
                            reader.beginArray();
                            while (reader.hasNext()) {
                                if (reader.peek() != null) {
                                    model.addCurrent_team(reader.nextString());
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endArray();
                            break;
                        case "positions":
                            reader.beginArray();
                            while (reader.hasNext()) {
                                if (reader.peek() != null) {
                                    model.addPosition(getPositionNameFromID(reader.nextString()));
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endArray();
                            break;
                        case "featured_media":
                            model.setFeatured_media(getImageFromFeatured(reader.nextString()));
                            break;
                        case "date":
                            model.setBirthDay(reader.nextString());
                            break;

                        default:
                            reader.skipValue();
                            break;
                    }
                } else {
                    reader.skipValue();
                }
            }


            //Second level end
            reader.endObject();

            //Adds the model to the array
            results.add(model);
        }

        //At main level end
        reader.endArray();

        return results;
    }


    private String parsePositionNameFromReader(JsonReader reader) throws IOException {
        String pos = "";

        reader.beginObject();

        while (reader.hasNext() && reader.peek() != null) {
            String curr_key = reader.nextName();
            switch (curr_key) {
                case "name":
                    pos = reader.nextString();
                    break;
                default:
                    reader.skipValue();
                    break;
            }

        }

        reader.endObject();

        return pos;
    }


    //Private helper methods
    private Reader sanitizeReader(InputStreamReader resReader) {
        InputStreamReader out;
        StringBuilder sb =  new StringBuilder();
        String line, sanitized;

        BufferedReader reader = new BufferedReader(resReader);

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                resReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        sanitized = removeScriptContent(sb.toString());
        out =  new InputStreamReader(new ByteArrayInputStream(sanitized.getBytes()));
        return out;

    }

    private String removeScriptContent(String html) {
        if(html != null) {
            String re = "<script .+>[\\s\\S]*<\\/script>";

            Pattern pattern = Pattern.compile(re);
            Matcher matcher = pattern.matcher(html);
            if (matcher.find()) {
                return html.replace(matcher.group(0), "").trim();
            }
        }
        return null;
    }
}
