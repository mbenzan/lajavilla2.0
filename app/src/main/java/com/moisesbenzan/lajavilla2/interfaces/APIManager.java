package com.moisesbenzan.lajavilla2.interfaces;

import android.content.Context;
import android.util.Log;

import com.moisesbenzan.lajavilla2.database.LeaguesDBOps;
import com.moisesbenzan.lajavilla2.database.PostsDBOps;
import com.moisesbenzan.lajavilla2.models.LeagueModel;
import com.moisesbenzan.lajavilla2.models.PostModel;
import com.moisesbenzan.lajavilla2.services.UpdateLocalDB;

import java.util.ArrayList;


public class APIManager {
    private static final String TAG = "ApiManager";
    private final Context c;

    public APIManager(Context context) {
        this.c = context;
    }

    public ArrayList<PostModel> getRecentPosts() {
        // TODO make this check a DB to see if up to date. If not then do net request.
        return getRecentPostsFromDB();
    }

    public ArrayList<LeagueModel> getRecentLeague() {
        // TODO make this check a DB to see if up to date. If not then do net request.
        return getRecentLeaguesFromDB();
    }

    public LeagueModel getLeagueFromID(String id) {
        return getLeagueFromID(id.trim(), false);
    }

    public ArrayList<String> getPostsTitleArray() {
        return getRecentPostsTitle();
    }

    public void updateAllDB() {
        UpdateLocalDB.startUpdateAllDB(this.c);
    }

    private LeagueModel getLeagueFromID(String id, Boolean fromMainThread) {
        LeaguesDBOps db = new LeaguesDBOps(this.c);
        LeagueModel model = db.getLeagueByID(id);
        if (model != null) {
            return model;
        } else {
            if (!fromMainThread) {
                WPApiCalls api = new WPApiCalls();
                model = api.getLeagueFromID(id);
            }
            return model;
        }
    }

    public String getLeagueNameFromID(String id, Boolean fromMainThread) {
        LeagueModel m = this.getLeagueFromID(id, fromMainThread);
        if (m != null) {
            return m.getName();
        } else {
            return "";
        }
    }


    private ArrayList<PostModel> getRecentPostsFromDB() {
        PostsDBOps dbOps = new PostsDBOps(this.c);
        ArrayList<PostModel> posts = dbOps.getAllPosts();
        Log.d(TAG, "Result # " + posts.size());

        if (posts.size() < 1) {
            UpdateLocalDB.startUpdateLocalPostsDB(this.c);
        }
        return posts;
    }

    private ArrayList<LeagueModel> getRecentLeaguesFromDB() {
        LeaguesDBOps dbOps = new LeaguesDBOps(this.c);
        ArrayList<LeagueModel> leagues = dbOps.getAllLeagues();

        Log.d(TAG, "Result # " + leagues.size());

        if (leagues.size() < 1) {
            UpdateLocalDB.startUpdateLocalLeaguesDB(this.c);
        }

        return leagues;

    }

    private ArrayList<String> getRecentPostsTitle() {
        PostsDBOps dbOps = new PostsDBOps(this.c);
        ArrayList<String> res = dbOps.getAllPostsTitles();
        return res;
    }


}
