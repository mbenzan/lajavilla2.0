package com.moisesbenzan.lajavilla2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.moisesbenzan.lajavilla2.adapters.ImageSliderAdapter;

import java.util.ArrayList;

public class NoticiasDetail extends AppCompatActivity {
    private static final String TAG = "NOTICIASDETAIL";


    private final ArrayList<String> img = new ArrayList<>();
    private String mPostUrl;

    private final static int placeHolderImage = R.drawable.ic_school_black_24dp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticias_detail);
        Bundle params = getIntent().getExtras();

        //Get all the handles
        CollapsingToolbarLayout mCollapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.detail_noticia_app_bar_collapsing_toolbar);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.noticia_detail_toolbar);
        ViewPager mImageSlider = (ViewPager) findViewById(R.id.detail_noticia_image_slider);
        TextView mTitle = (TextView) findViewById(R.id.detail_noticia_title);
        TextView mAuthor = (TextView) findViewById(R.id.detail_noticia_author);
        TextView mContent = (TextView) findViewById(R.id.detail_noticia_content);
        TextView mDate = (TextView) findViewById(R.id.detail_noticia_date);


        //Sets the collapsing toolbar properties
        //mCollapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.primary_text));
        mCollapsingToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        mCollapsingToolbar.setTitle("");

        //Sets the normal toolbar properties
        mToolbar.setTitleTextColor(getResources().getColor(R.color.primary_text));
        mToolbar.setTitle("");

        //Method that will be true 99% of the times. Will fill in the view based on parameters
        //passed on the Intent.
        if (!params.isEmpty()) {

            //Gets the post URL
            mPostUrl = params.getString("PostUrl");

            //Sets the title on the content
            mTitle.setText(params.getString("Title"));

            //Sets the author of the content
            mAuthor.setText(params.getString("Author"));

            //Sets the content body
            mContent.setText(params.getString("Content"));

            //Sets the date
            mDate.setText(params.getString("Date"));

            //Gets the image from the intent
            String primaryImage = params.getString("PrimaryImage");
            img.add(primaryImage);
            for (String s : params.getStringArrayList("ImgUrl")) {
                if (!s.equals(primaryImage)) {
                    img.add(s);
                }
            }

            //Updates the adapter and passes it to the viewpager
            ImageSliderAdapter adapter = new ImageSliderAdapter(this, img);
            mImageSlider.setAdapter(adapter);

        }

    }


    public void shareAction(View v) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, mPostUrl);
        startActivity(Intent.createChooser(intent, getString(R.string.share_message)));
    }
}
