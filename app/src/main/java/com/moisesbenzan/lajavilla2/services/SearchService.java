package com.moisesbenzan.lajavilla2.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.moisesbenzan.lajavilla2.PlayerDetail;
import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.fragments.PlayerSearchResults;
import com.moisesbenzan.lajavilla2.interfaces.WPApiCalls;
import com.moisesbenzan.lajavilla2.models.PlayerModel;

import java.util.ArrayList;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SearchService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_SEARCH_PLAYER = "com.moisesbenzan.lajavilla2.services.action.SEARCH_PLAYER";

    //Private service TAG
    private final static String TAG = "SearchService";


    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.moisesbenzan.lajavilla2.services.extra.PLAYERNAME";

    public SearchService() {
        super("SearchService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startSearchForPlayer(Context context, String playerName) {
        Intent intent = new Intent(context, SearchService.class);
        intent.setAction(ACTION_SEARCH_PLAYER);
        intent.putExtra(EXTRA_PARAM1, playerName);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SEARCH_PLAYER.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                searchForPlayer(param1);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void searchForPlayer(String playerName) {
        //Searches for the player
        WPApiCalls apiCalls = new WPApiCalls();

        showToastInUiThread(this, getString(R.string.search_wait));
        ArrayList<PlayerModel> results = apiCalls.getPlayerInfo(playerName);

        if (results.size() < 1) {
            String message = getString(R.string.search_no_results).replace("{}", playerName);
            showToastInUiThread(this, message);
        } else if (results.size() == 1){

            //Configures the new intent
            Intent intent = new Intent(this, PlayerDetail.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            Bundle args = new Bundle();
            args.putSerializable("Player", results.get(0));

            //Sets the arguments
            intent.putExtras(args);

            //Starts the activity
            startActivity(intent);
        }
        else {
            //Configures intent to search results
            Intent intent = new Intent(this, PlayerSearchResults.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            //Configures the intent arguments
            intent.putExtra("Results", results);

            //Starts the activity
            startActivity(intent);

        }
    }


    private static void showToastInUiThread(final Context ctx,
                                            final String message) {

        Handler mainThread = new Handler(Looper.getMainLooper());
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
            }
        });
    }

}
