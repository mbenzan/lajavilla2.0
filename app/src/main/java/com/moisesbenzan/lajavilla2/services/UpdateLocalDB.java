package com.moisesbenzan.lajavilla2.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.moisesbenzan.lajavilla2.MainActivity;
import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.database.LeaguesDBOps;
import com.moisesbenzan.lajavilla2.database.PostsDBOps;
import com.moisesbenzan.lajavilla2.interfaces.WPApiCalls;
import com.moisesbenzan.lajavilla2.models.LeagueModel;
import com.moisesbenzan.lajavilla2.models.PostModel;

import java.util.ArrayList;

public class UpdateLocalDB extends IntentService {

    private static final String TAG = "UPDATEDBSERVICE";
    private static final int NOTIFICATIONID = 71898;

    private static final String ACTION_UPDATE_LOCAL_POSTS_DB = "com.moisesbenzan.lajavilla2.services.action.UPDATE_LOCAL_POSTS_DB";
    private static final String ACTION_UPDATE_LOCAL_LEAGUES_DB = "com.moisesbenzan.lajavilla2.services.action.UPDATE_LOCAL_LEAGUES_DB";
    private static final String ACTION_UPDATE_LOCAL_ALL_DB = "com.moisesbenzan.lajavilla2.services.action.UPDATE_LOCAL_ALL_DB";
    private Boolean update_all = false;

    public UpdateLocalDB() {
        super("UpdateLocalDB");
    }

    //Helper methods
    public static void startUpdateLocalPostsDB(Context context) {
        Intent intent = new Intent(context, UpdateLocalDB.class);
        intent.setAction(ACTION_UPDATE_LOCAL_POSTS_DB);
        context.startService(intent);
    }

    public static void startUpdateLocalLeaguesDB(Context context) {
        Intent intent = new Intent(context, UpdateLocalDB.class);
        intent.setAction(ACTION_UPDATE_LOCAL_LEAGUES_DB);
        context.startService(intent);
    }

    public static void startUpdateAllDB(Context context) {
        Intent intent = new Intent(context, UpdateLocalDB.class);
        intent.setAction(ACTION_UPDATE_LOCAL_ALL_DB);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_UPDATE_LOCAL_POSTS_DB:
                    UpdateLocalPostsDB();
                    break;
                case ACTION_UPDATE_LOCAL_LEAGUES_DB:
                    UpdateLocalLeaguesDB();
                    break;
                case ACTION_UPDATE_LOCAL_ALL_DB:
                    UpdateAllDB();
                    break;
            }
        }
    }

    // Gets executed in the background

    private void UpdateAllDB() {
        this.update_all = true;

        UpdateLocalPostsDB();
        UpdateLocalLeaguesDB();
        sendDoneBroadcast(100);
    }

    private void UpdateLocalPostsDB() {
        WPApiCalls api = new WPApiCalls();
        ArrayList<PostModel> new_posts = api.getRecentPosts();

        //Inserts to db
        PostsDBOps db = new PostsDBOps(this);
        Integer size = db.insertBatchPosts(new_posts); //Returns the new posts size

        //Log to notify update complete.
        if (!this.update_all) {
            sendDoneBroadcast(1);
        }
        Log.d(TAG, "Posts DB update complete.");

        if (size > 0) {
            //Sends the notification only if there are new items
            String msg = getString(R.string.posts_notification_message).replace("{}", size.toString());
            Intent intent = new Intent(this, MainActivity.class);
            sendNotification(msg, intent);
        }
    }

    private void UpdateLocalLeaguesDB() {
        //Gets resource from server
        WPApiCalls api = new WPApiCalls();
        ArrayList<LeagueModel> new_leagues = api.getLeagues();

        //Inserts to db
        LeaguesDBOps db = new LeaguesDBOps(this);
        db.insertBatchLeagues(new_leagues);

        //notify done
        if (!this.update_all) {
            sendDoneBroadcast(2);
        }
        Log.d(TAG, "Leagues DB update complete.");

    }

    private void sendDoneBroadcast(int notification_code) {
        String br;
        switch (notification_code) {
            case 1:
                // Only posts update
                br = getString(R.string.POSTSUPDATEBROADCAST);
                break;
            case 2:
                //Only leagues updated
                br = getString(R.string.LEAGUESUPDATEBROADCAST);
                break;
            case 100:
                //All DBs were updated
                br = getString(R.string.ALLUPDATEBROADCAST);
                Log.d(TAG, "Sending all update broadcast.");
                break;
            default:
                br = getString(R.string.ALLUPDATEBROADCAST);
                Log.d(TAG, "Sending all update broadcast.");
        }

        Intent intent = new Intent(br);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendNotification(String message, Intent intent) {
        //Creates the builder
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        //Sets the icon
        mBuilder.setSmallIcon(R.drawable.lajavillasplash);

        //Sets the title
        mBuilder.setContentTitle(getString(R.string.app_name));

        //Sets the content
        mBuilder.setContentText(message);

        //Creates the pending intent
        PendingIntent resPending = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Sets the intent
        mBuilder.setContentIntent(resPending);

        //Gets a hold of the notification manager
        NotificationManager mManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        //Issues the notification
        mManager.notify(NOTIFICATIONID, mBuilder.build());


    }

}
