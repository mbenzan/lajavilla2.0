package com.moisesbenzan.lajavilla2.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;


/**
 * Created by User on 4/11/2017.
 */

public class AlarmReceiver extends BroadcastReceiver {
    private final static String TAG = "AlarmReceiver";


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            if (action.equals("android.intent.action.BOOT_COMPLETED")) {
                setAlarm(context);
            } else if (action.equals("UPDATEALL")) {
                UpdateLocalDB.startUpdateAllDB(context);
            }
        }
    }


    private void setAlarm(Context c) {
        //Sets up the recurrent alarm if it is not already set
        //Alarm will run twice a day
        Intent alarm = new Intent(c, AlarmReceiver.class);
        alarm.setAction("UPDATEALL");
        boolean alarmRunning = (PendingIntent.getBroadcast(c, 0, alarm, PendingIntent.FLAG_NO_CREATE) != null);
        if (!alarmRunning) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(c, 0, alarm, 0);
            AlarmManager alarmManager = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_HALF_DAY, AlarmManager.INTERVAL_HALF_DAY, pendingIntent);
            Log.d(TAG, "Registered recurring alarm on boot.");
        }
    }
}
