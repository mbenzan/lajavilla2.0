package com.moisesbenzan.lajavilla2.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.fragments.WebViewActivity;
import com.moisesbenzan.lajavilla2.models.LeagueModel;

import java.util.ArrayList;

/**
 * Created by User on 3/23/2017.
 */

public class LeaguesAdapter extends RecyclerView.Adapter<LeaguesAdapter.ViewHolder> {

    private final ArrayList<LeagueModel> mDataset;
    private final Context mContext;
    private final String TAG = "LeaguesAdapter";

    public LeaguesAdapter(Context c, ArrayList<LeagueModel> leagues) {
        this.mContext = c;
        this.mDataset = leagues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leagues_card_style, parent, false);
        // set the view's size, margins, paddings and layout parameters

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Sets the action to be taken on click to the view.
                TextView mText = (TextView) view.findViewById(R.id.league_card_view_title);
                LeagueModel model = (LeagueModel) mText.getTag();

                Intent intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra("Url", model.getLink());
                mContext.startActivity(intent);
            }
        });

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LeagueModel league = mDataset.get(position);

        holder.mTitle.setText(league.getName());
        holder.mTitle.setTag(league);
    }

    @Override
    public int getItemCount() {

        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mTitle;

        public ViewHolder(View v) {
            super(v);
            mTitle = (TextView) v.findViewById(R.id.league_card_view_title);


        }
    }
}
