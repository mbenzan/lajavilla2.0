package com.moisesbenzan.lajavilla2.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moisesbenzan.lajavilla2.R;

import java.util.ArrayList;

/**
 * Created by jovin.pj on 29-07-2015.
 */
public class MarqueeScrollingAdapter extends RecyclerView.Adapter<MarqueeScrollingAdapter.ViewHolder> {

    private ArrayList<String> mDataset;

    public MarqueeScrollingAdapter(ArrayList<String> titles) {
        this.mDataset = titles;
    }

    @Override
    public MarqueeScrollingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.marquee_title_style, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTitle.setText(mDataset.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    // Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public final TextView mTitle;

        public ViewHolder(View v) {
            super(v);
            this.mTitle = (TextView) v.findViewById(R.id.marquee_title_text);
        }
    }

}
