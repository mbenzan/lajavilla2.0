package com.moisesbenzan.lajavilla2.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.moisesbenzan.lajavilla2.PlayerDetail;
import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.models.PlayerModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 4/12/2017.
 */

public class PlayerSearchAdapter extends RecyclerView.Adapter<PlayerSearchAdapter.ViewHolder> {
    private final Context mContext;
    private final ArrayList<PlayerModel> dataset;

    //Constructors
    public PlayerSearchAdapter(Context c, ArrayList<PlayerModel> data){
        this.mContext = c;
        this.dataset = data;
    }

    @Override
    public PlayerSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player_results_card_style, parent, false);

        //Sets the onclickListener
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Gets the tag from the view
                TextView mName = (TextView) view.findViewById(R.id.player_results_name);
                PlayerModel model = (PlayerModel) mName.getTag();

                //Starts the detail activity from the object in the tag
                //Configures the new intent
                Intent intent = new Intent(mContext, PlayerDetail.class);
                Bundle args = new Bundle();
                args.putSerializable("Player", model);

                //Sets the arguments
                intent.putExtras(args);

                //Starts the activity
                mContext.startActivity(intent);
            }
        });

        //Returns the view
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Gets the element at position of dataser
        PlayerModel model = this.dataset.get(position);

        //Sets the values in the view

        //Sets the player name
        holder.mName.setText(model.getName());

        //Sets the positions in a comma separated list
        holder.mPosition.setText(model.getPositions());

        //Loads the image
        Picasso.with(this.mContext)
                .load(model.getFeatured_media())
                .fit()
                .centerInside()
                .into(holder.mImageView);

        //Sets the object as a tag to the view so it can be accessed by the click listener
        holder.mName.setTag(model);

    }

    @Override
    public int getItemCount() {
        return this.dataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mName;
        final TextView mPosition;
        final ImageView mImageView;

        public ViewHolder(View v) {
            super(v);
            mName = (TextView) v.findViewById(R.id.player_results_name);
            mImageView = (ImageView) v.findViewById(R.id.player_results_imageView);
            mPosition = (TextView) v.findViewById(R.id.player_results_position);

        }
    }
}
