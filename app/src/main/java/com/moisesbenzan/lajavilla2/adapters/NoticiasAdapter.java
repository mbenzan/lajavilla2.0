package com.moisesbenzan.lajavilla2.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.moisesbenzan.lajavilla2.NoticiasDetail;
import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.models.PostModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by User on 2/19/2017.
 */

public class NoticiasAdapter extends RecyclerView.Adapter<NoticiasAdapter.ViewHolder> {

    private final ArrayList<PostModel> mDataset;
    private final Context mContext;

    //Default drawable for picasso
    private final static int placeholderImage = R.drawable.ic_school_black_24dp;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mTitle;
        public final TextView mAuthor;
        public final TextView mDate;
        public final ImageView mImageView;

        public ViewHolder(View v) {
            super(v);
            mTitle = (TextView) v.findViewById(R.id.card_view_title);
            mDate = (TextView) v.findViewById(R.id.card_view_date);
            mImageView = (ImageView) v.findViewById(R.id.card_view_image);
            mAuthor = (TextView) v.findViewById(R.id.card_view_author);

        }
    }

    public NoticiasAdapter(Context c, ArrayList<PostModel> myDataset) {
        mDataset = myDataset;
        mContext = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NoticiasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.noticias_card_style, parent, false);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Sets the action to be taken on click to the view.
                TextView mTitle = (TextView) view.findViewById(R.id.card_view_title);
                PostModel model = (PostModel) mTitle.getTag();

                Bundle values = new Bundle();
                values.putString("Title", model.getTitle());
                values.putString("Content", model.getFormattedContent());
                values.putString("Author", model.getAuthor().getName());
                values.putString("Date", model.getFormattedDate());
                values.putStringArrayList("ImgUrl", model.getImageURLArray());
                values.putString("PostUrl", model.getLink());
                values.putString("PrimaryImage", model.getFirstImgURL());

                Intent intent = new Intent(mContext, NoticiasDetail.class);
                intent.putExtras(values);
                mContext.startActivity(intent);
            }
        });

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        PostModel p = mDataset.get(position);
        holder.mTitle.setText(p.getTitle());
        holder.mAuthor.setText(p.getAuthor().getName());
        holder.mDate.setText(p.getFormattedDate());

        //Sets the image
        String img = p.getFirstImgURL();

        String TAG = "NoticiasAdapter";
        Log.d(TAG, "Loding image: " + img);

        if (img.isEmpty()) {
            Picasso.with(mContext)
                    .load(placeholderImage)
                    .fit()
                    .centerInside()
                    .into(holder.mImageView);
        } else {
            Picasso.with(mContext)
                    .load(img)
                    .fit()
                    .centerInside()
                    .into(holder.mImageView);
        }

        //Sets the object for later processing
        holder.mTitle.setTag(p);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
