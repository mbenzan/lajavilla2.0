package com.moisesbenzan.lajavilla2.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.fragments.FullScreenImage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 3/26/2017.
 */

public class ImageSliderAdapter extends PagerAdapter {
    private final static String TAG = "ImageSliderAdapter";
    private final ArrayList<String> mDataset;
    private final Context mContext;
    private final LayoutInflater mLayoutInflater;

    private static final int mImagePlaceHolder = R.drawable.ic_school_black_24dp;

    public ImageSliderAdapter(Context c, ArrayList<String> mResources) {
        this.mContext = c;
        this.mDataset = mResources;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        String current_img = mDataset.get(position);
        View v = mLayoutInflater.inflate(R.layout.image_slider_item, container, false);
        v.setTag(current_img);

        ImageView imageView = (ImageView) v.findViewById(R.id.slider_imageview);

        Log.d(TAG, "Current img: " + current_img);
        //Adds the image
        Picasso.with(mContext)
                .load(current_img)
                .fit()
                .centerInside()
                .error(mImagePlaceHolder)
                .into(imageView);

        //Sets a click listener on each image
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String img = (String) view.getTag();
                Intent intent = new Intent(mContext, FullScreenImage.class);
                intent.putExtra("Image", img);
                mContext.startActivity(intent);
            }
        });

        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
