package com.moisesbenzan.lajavilla2.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.moisesbenzan.lajavilla2.fragments.AboutFragment;
import com.moisesbenzan.lajavilla2.fragments.LeaguesFragment;
import com.moisesbenzan.lajavilla2.fragments.NoticiasFragment;

/**
 * Created by User on 3/22/2017.
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {

    private final String[] tabTitle = new String[]{"Noticias", "Torneos y Ligas", "Sobre Nosotros"};

    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int pos) {
        return this.tabTitle[pos];
    }

    @Override
    public Fragment getItem(int position) {
        //Here we decide which fragment gets called
        Fragment fr;
        switch (position) {
            case 0:
                fr = new NoticiasFragment();
                break;
            case 1:
                fr = new LeaguesFragment();
                break;
            case 2:
                fr = new AboutFragment();
                break;
            default:
                fr = new NoticiasFragment();
                break;

        }

        return fr;
    }

    @Override
    public int getCount() {
        //return this.tabTitle.length;
        return 3;
    }
}
