package com.moisesbenzan.lajavilla2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class LeagueDetail extends AppCompatActivity {

    private String mLeagueUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_league_detail);

        //Gets the passed bundle
        Bundle params = getIntent().getExtras();

        //Gets all the handlers
        Toolbar mToolbar = (Toolbar) findViewById(R.id.detail_league_toolbar);

        //Sets the normal toolbar properties
        mToolbar.setTitleTextColor(getResources().getColor(R.color.primary_text));
        mToolbar.setTitle("");

        if (params != null) {
            //If bundle is not null then construct

            //Gets the link from the bundle
            mLeagueUrl = params.getString("Link");

            //Sets the title
            mToolbar.setTitle(params.getString("Name"));
        }


    }


    public void LeagueshareAction(View v) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, mLeagueUrl);
        startActivity(Intent.createChooser(intent, getString(R.string.share_message)));
    }
}
