package com.moisesbenzan.lajavilla2.fragments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.adapters.PlayerSearchAdapter;
import com.moisesbenzan.lajavilla2.models.PlayerModel;

import java.util.ArrayList;

public class PlayerSearchResults extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_search_results);
        Bundle params = getIntent().getExtras();

        //Gets all the handlers
        Toolbar mToolbar = (Toolbar) findViewById(R.id.player_results_toolbar);

        if(params != null) {

            //Sets the normal toolbar properties
            mToolbar.setTitleTextColor(getResources().getColor(R.color.primary_text));
            mToolbar.setTitle(R.string.SearchResultTitle);

            //configures the toolbar
            setSupportActionBar(mToolbar);


            ArrayList<PlayerModel> data = (ArrayList<PlayerModel>) params.getSerializable("Results");

            //Sets up RecyclerView Handler
            RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.PlayerSearchResultsRecyclerView);
            mRecyclerView.setHasFixedSize(true); //Improves performance

            //Adds data and sets the adapter of the RecyclerView
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);

            //Adds the dataset to the adapter
            PlayerSearchAdapter adapter = new PlayerSearchAdapter(this, data);

            //Sets the adapter to the RecyclerView
            mRecyclerView.setAdapter(adapter);

        }

    }
}
