package com.moisesbenzan.lajavilla2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.adapters.NoticiasAdapter;
import com.moisesbenzan.lajavilla2.interfaces.APIManager;
import com.moisesbenzan.lajavilla2.models.PostModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link NoticiasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NoticiasFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    //Runtime Variables

    public NoticiasFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NoticiasFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NoticiasFragment newInstance(Context mainContext) {
        return new NoticiasFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View finalView = inflater.inflate(R.layout.fragment_noticias, container, false);


        //Sets up RecyclerView Handler
        RecyclerView mRecyclerView = (RecyclerView) finalView.findViewById(R.id.NoticiasRecyclerView);
        mRecyclerView.setHasFixedSize(true); //Improves performance

        //Adds data and sets the adapter of the RecyclerView
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Gets an API Handler
        APIManager mApi = new APIManager(getActivity());
        //Gets the dataset
        ArrayList<PostModel> data = mApi.getRecentPosts();

        //Adds the datatset to the adapter
        NoticiasAdapter adapter = new NoticiasAdapter(getContext(), data);

        //Sets the adapter to the RecyclerView
        mRecyclerView.setAdapter(adapter);

        // Inflate the layout for this fragment
        return finalView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
