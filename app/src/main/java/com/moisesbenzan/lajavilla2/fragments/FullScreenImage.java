package com.moisesbenzan.lajavilla2.fragments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.moisesbenzan.lajavilla2.R;
import com.squareup.picasso.Picasso;

public class FullScreenImage extends AppCompatActivity {

    private static final int mImagePlaceHolder = R.drawable.ic_school_black_24dp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_slider_item);
        Bundle b = getIntent().getExtras();

        if (b != null) {
            //Get a handle of the ImageView
            ImageView mImage = (ImageView) findViewById(R.id.slider_imageview);

            //Sets the image into the view using Picasso
            Picasso.with(this)
                    .load(b.getString("Image"))
                    .fit()
                    .centerInside()
                    .error(mImagePlaceHolder)
                    .into(mImage);

        }


    }
}
