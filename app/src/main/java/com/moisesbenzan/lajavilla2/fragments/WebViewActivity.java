package com.moisesbenzan.lajavilla2.fragments;

import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.DownloadListener;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.moisesbenzan.lajavilla2.R;

public class WebViewActivity extends AppCompatActivity {

    private final static String TAG = "WebViewActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        Bundle params = getIntent().getExtras();

        //Get all the handlers
        WebView mWebView = (WebView) findViewById(R.id.activity_webview);

        if (params != null) {
            //If not empty then get the player
            String mUrl = params.getString("Url");

            //Loads the url into the webview
            mWebView.loadUrl(mUrl);
            Log.d(TAG, "Loading url: " + mUrl);

            //Enables javascript
            mWebView.getSettings().setJavaScriptEnabled(true);

            //Enables same site link redirection
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest url) {
                    if (url.getUrl().getHost().equals("www.academialajavilla.com")) {
                        // It is our website, so let the url get loaded in the same webView
                        return false;
                    }

                    Intent intent = new Intent(Intent.ACTION_VIEW, url.getUrl());
                    startActivity(intent);
                    return true;
                }
            });

            // Enables download of PDF content

            mWebView.setDownloadListener(new DownloadListener() {
                @Override
                public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                    Request download = new Request(Uri.parse(url));

                    //Sets download options
                    download.allowScanningByMediaScanner();
                    download.setNotificationVisibility(Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    download.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, Uri.parse(url).getLastPathSegment());

                    //Starts the download
                    DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    dm.enqueue(download);

                    String download_message = getString(R.string.download_notification).replace("{}", Uri.parse(url).getLastPathSegment());
                    Toast.makeText(WebViewActivity.this, download_message, Toast.LENGTH_LONG).show();
                }
            });


        }
    }
}
