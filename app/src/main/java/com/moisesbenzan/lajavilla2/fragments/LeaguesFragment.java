package com.moisesbenzan.lajavilla2.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moisesbenzan.lajavilla2.R;
import com.moisesbenzan.lajavilla2.adapters.LeaguesAdapter;
import com.moisesbenzan.lajavilla2.interfaces.APIManager;
import com.moisesbenzan.lajavilla2.models.LeagueModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LeaguesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LeaguesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    public LeaguesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LeaguesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LeaguesFragment newInstance(String param1, String param2) {
        LeaguesFragment fragment = new LeaguesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View finalView = inflater.inflate(R.layout.fragment_leagues, container, false);


        //Sets up RecyclerView Handler
        RecyclerView mRecyclerView = (RecyclerView) finalView.findViewById(R.id.LeaguesRecyclerViewer);
        mRecyclerView.setHasFixedSize(true); //Improves performance

        //Adds data and sets the adapter of the RecyclerView
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Gets an API Handler
        APIManager mApi = new APIManager(getActivity());
        //Gets the dataset
        ArrayList<LeagueModel> data = mApi.getRecentLeague();

        //Adds the datatset to the adapter
        LeaguesAdapter adapter = new LeaguesAdapter(getContext(), data);

        //Sets the adapter to the RecyclerView
        mRecyclerView.setAdapter(adapter);

        // Inflate the layout for this fragment
        return finalView;
    }

}
