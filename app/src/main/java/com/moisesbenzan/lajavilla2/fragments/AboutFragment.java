package com.moisesbenzan.lajavilla2.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.moisesbenzan.lajavilla2.R;

import java.util.Calendar;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class AboutFragment extends Fragment {


    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Element adsElement = new Element();

        return new AboutPage(getContext())
                .isRTL(false)
                .setDescription("Somos una institución de clase y estándares internacionales, dedicada al desarrollo y a la promoción del béisbol de Pequeña Ligas y Juvenil. Buscando siempre el desarrollo integral de nuestros niños y adolescentes, tanto en el plano deportivo como social, con el objetivo de contribuir a crear las bases para una mejor sociedad.")
                .setImage(R.drawable.lajavillasplash)
                .addItem(new Element().setTitle("Version 1.0"))
                .addItem(adsElement)
                .addGroup("Connect with us")
                .addEmail("academialajavilla@gmail.com")
                .addWebsite("http://academialajavilla.com/")
                .addFacebook("javillero")
                .addTwitter("Academiajavilla")
                .addInstagram("academialajavilla")
                .addItem(getCopyRightsElement())
                .create();
    }


    private Element getCopyRightsElement() {
        Element copyRightsElement = new Element();
        final String copyrights = String.format(getString(R.string.copy_right), Calendar.getInstance().get(Calendar.YEAR));
        copyRightsElement.setTitle(copyrights);
        copyRightsElement.setIcon(R.drawable.about_icon_copy_right);
        copyRightsElement.setColor(ContextCompat.getColor(getContext(), mehdi.sakout.aboutpage.R.color.about_item_icon_color));
        copyRightsElement.setGravity(Gravity.CENTER);
        copyRightsElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), copyrights, Toast.LENGTH_SHORT).show();
            }
        });
        return copyRightsElement;
    }

}
