package com.moisesbenzan.lajavilla2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.moisesbenzan.lajavilla2.fragments.FullScreenImage;
import com.moisesbenzan.lajavilla2.fragments.WebViewActivity;
import com.moisesbenzan.lajavilla2.models.PlayerModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PlayerDetail extends AppCompatActivity {
    private static final String TAG = "PlayerDetail";
    private static final int mImagePlaceHolder = R.drawable.ic_school_black_24dp;
    private String mPlayerUrl;
    private FloatingActionsMenu mFabMenu;
    private FloatingActionButton mActionShare, mActionStats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_detail);

        Bundle params = getIntent().getExtras();

        //Gets all the handlers
        Toolbar mToolbar = (Toolbar) findViewById(R.id.detail_player_toolbar);
        TextView mBirthPlace = (TextView) findViewById(R.id.player_detail_birthplace);
        TextView mNationality = (TextView) findViewById(R.id.player_detail_nationality);
        TextView mCategory = (TextView) findViewById(R.id.player_detail_category);
        TextView mBT = (TextView) findViewById(R.id.player_detail_BT);
        TextView mPrevTeams = (TextView) findViewById(R.id.player_detail_prevTeams);
        TextView mBirthDay = (TextView) findViewById(R.id.player_detail_birthDay);
        ImageView mBackDrop = (ImageView) findViewById(R.id.detail_player_backdrop);
        LinearLayout mPositions = (LinearLayout) findViewById(R.id.player_detail_positionLayout);
        LinearLayout mTournaments = (LinearLayout) findViewById(R.id.player_detail_tournamentsLayout);
        mFabMenu = (FloatingActionsMenu) findViewById(R.id.player_detail_fab_menu);
        mActionShare = (FloatingActionButton) findViewById(R.id.player_detail_fab_action_share);
        mActionStats = (FloatingActionButton) findViewById(R.id.player_detail_fab_action_stats);


        //Sets the normal toolbar properties
        mToolbar.setTitleTextColor(getResources().getColor(R.color.primary_text));
        mToolbar.setTitle("");

        if (params != null) {
            //If bundle is not null then construct

            mActionStats.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mFabMenu.isExpanded()) {
                        mFabMenu.collapse();
                        PlayerShowStats(view);
                    }
                }
            });

            mActionShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mFabMenu.isExpanded()) {
                        mFabMenu.collapse();
                        PlayerSearchActionShare(view);
                    }
                }
            });

            //Gets the playerModel from the bundle
            final PlayerModel player = (PlayerModel) params.getSerializable("Player");

            //Stores the player model
            PlayerModel mPlayer = player;

            //Sets the player URL for share
            mPlayerUrl = player.getLink();

            //Sets the title
            mToolbar.setTitle(player.getName());

            //Sets the birthplace
            mBirthPlace.setText(player.getBirth_place());

            //Sets the nationality
            mNationality.setText(player.getNationality());

            //Sets the category
            mCategory.setText(player.getCategory());

            //Sets the Bats and Throws
            mBT.setText(player.getBT());

            //Sets the previous teams
            mPrevTeams.setText(player.getPast_teams());

            //Sets the birthday
            String bday = getString(R.string.bday_format).replace("{bday}", player.getFormattedBirthday()).replace("{age}", player.getAge());
            mBirthDay.setText(bday);


            //Sets the player image
            Picasso.with(this)
                    .load(player.getFeatured_media())
                    .fit()
                    .centerInside()
                    .error(mImagePlaceHolder)
                    .into(mBackDrop);

            mBackDrop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getBaseContext(), FullScreenImage.class);
                    intent.putExtra("Image", player.getFeatured_media());
                    startActivity(intent);
                }
            });

            //Sets the positions
            ArrayList<String> positions = player.getPositionsList();
            for (String pos : positions) {
                TextView tv = new TextView(this);
                tv.setText(pos);
                tv.setLayoutParams(new LayoutParams(
                        LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT
                ));

                tv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.bullet, 0, 0, 0);
                tv.setCompoundDrawablePadding(20);
                tv.setPadding(0, 0, 0, 10); //Left, Top, Right, Bottom


                mPositions.addView(tv);
            }

            //Sets the tournaments
            ArrayList<String> tournaments = player.getTournamentsList();


            for (String t : tournaments) {
                //Adds the to view
                TextView tv = new TextView(this);
                tv.setText(t);
                tv.setLayoutParams(new LayoutParams(
                        LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT
                ));

                tv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.bullet, 0, 0, 0);
                tv.setCompoundDrawablePadding(20);
                tv.setPadding(0, 0, 0, 10); //Left, Top, Right, Bottom

                mTournaments.addView(tv);
            }


        }
    }


    public void PlayerSearchActionShare(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, mPlayerUrl);
        startActivity(Intent.createChooser(intent, getString(R.string.share_message)));
    }

    public void PlayerShowStats(View view) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("Url", mPlayerUrl);
        startActivity(intent);
    }

}
